<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'first',
            'alt' => 'first',
            'img' => 'first.png',
            'user_id' => '1'
        ]);
        DB::table('posts')->insert([
            'title' => 'second',
            'alt' => 'second',
            'img' => 'second.png',
            'user_id' => '2'
        ]);
        DB::table('posts')->insert([
            'title' => 'third',
            'alt' => 'third',
            'img' => 'third.png',
            'user_id' => '3'
        ]);
    }
}
