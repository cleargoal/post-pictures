<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User1',
            'surname' => 'Surname',
            'email' => 'user1@gmail.com',
            'password' => Hash::make('password1'),
        ]);
        DB::table('users')->insert([
            'name' => 'User2',
            'surname' => 'Surname',
            'email' => 'user2@gmail.com',
            'password' => Hash::make('password2'),
        ]);
        DB::table('users')->insert([
            'name' => 'User3',
            'surname' => 'Surname',
            'email' => 'user3@gmail.com',
            'password' => Hash::make('password3'),
        ]);
    }
}
