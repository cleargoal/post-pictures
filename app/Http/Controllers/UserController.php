<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return Application|Factory|View|Response
     */
    public function profile()
    {
        return view('users.profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|Factory|View|Response
     */
    public function edit()
    {
        return view('users.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        Validator::make($request->all(), [
            'name'     => 'required|string',
        ])->validate();

        $user = Auth::user();

        $user->name = $request->name;
        $user->surname = $request->surname;

        $user->save();

        return redirect()->route('user.profile')->with('success','User is updated successfully.');
    }

}
