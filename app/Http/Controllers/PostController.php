<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $posts = DB::table('posts')->simplePaginate(3);

        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        Validator::make($request->all(), [
            'title'     => 'required|string',
            'alt'       => 'required|string',
            'img'       => 'file|image'
        ])->validate();

        $post = new Post;
        $post->title = $request->title;
        $post->alt = $request->alt;

        $file_name = hash('sha256', time()).'.'.$request->img->extension();
        $request->img->move(storage_path('app\public\images'), $file_name);

        $post->img = $file_name;

        $user = Auth::user();

        $user->posts()->save($post);

        return redirect()->route('user.profile')->with('success','Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return Application|Factory|View|Response
     */
    public function show(Post $post)
    {
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(Post $post)
    {
        if(Auth::user()->id != $post->user_id){
            return redirect()->route('posts.index')->with('error','You have no rights to edit this post');
        }
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Post $post)
    {
        Validator::make($request->all(), [
            'title'     => 'required|string',
            'alt'       => 'required|string',
            'img'       => 'file|image'
        ])->validate();

        $post->title = $request->title;
        $post->alt = $request->alt;

        if($request->hasFile('img')){
            $old_img = $post->img;

            $file_name = hash('sha256', time()).'.'.$request->img->extension();
            $request->img->move(storage_path('app\public\images'), $file_name);
            $post->img = $file_name;

            Storage::delete(storage_path('app\public\images'. $old_img));
        }

        $post->save();

        return redirect()->route('user.profile')->with('success','Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Post $post): RedirectResponse
    {
        if(Auth::user()->id != $post->user_id){
            return redirect()->route('posts.index')->with('error','You have no rights to delete this post');
        }

        Storage::delete(public_path('images'. $post->img));
        $post->delete();

        return redirect()->back()->with('success','Post deleted successfully');
    }
}
