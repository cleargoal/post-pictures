<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $userPosts = DB::table('posts')
            ->select('id', 'title')
            ->where('user_id', $this->id)
            ->orderBy('created_at', 'desc')
            ->get();
        $postCount = $userPosts->count();
        $lastPost = $userPosts->first();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'post_count' => $postCount,
            'last_post_title' => $lastPost->title,
            'last_post_link' => url('/posts/' . $lastPost->id),
        ];
    }

}
