<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Profile') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ route('user.update') }}" method="post">
                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="name"><strong>Name:</strong></label>
                                            <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" placeholder="name"><br>
                                            <label for="surname"><strong>Surname:</strong></label>
                                            <input type="text" name="surname" value="{{ Auth::user()->surname }}" class="form-control" placeholder="surname"><br>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a class="btn btn-primary" href="{{ url()->previous() }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
