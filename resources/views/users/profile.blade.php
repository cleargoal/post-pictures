<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @elseif($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-5">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <p><strong>Name:</strong> {{ Auth::user()->name }}</p><br>
                            <p><strong>Surname:</strong> {{ Auth::user()->surname }}</p><br>
                            <p><strong>Email:</strong> {{ Auth::user()->email }}</p><br>
                            <a class="btn btn-primary" href="{{ route('user.edit') }}">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn btn-success mb-5" href="{{ route('posts.create') }}">New Post</a>
            @foreach (Auth::user()->posts as $post)
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-3">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <div class="row" style="margin-bottom: 2rem;">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <img style="width: 100%;" src="{{asset('storage/images') . '/' .  $post->img}}" alt="{{ $post->alt }}">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <p><strong>Title:</strong> {{ $post->title }}</p><br>
                                <p><strong>Alt:</strong> {{ $post->alt }}</p><br>
                                @if(Auth::user())
                                    <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                                        <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-app-layout>

