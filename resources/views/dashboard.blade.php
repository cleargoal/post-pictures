<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-3">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p id="original"></p>
                    <ul>
                        <li>Name:  <code id="name"></code></li>
                        <li>Surname: <code id="surname"></code></li>
                        <li>Post count: <code id="post_count"></code></li>
                        <li>Last post title: <code id="last_post_title"></code></li>
                        <li>Last post link: <a id="link_href" href=""><code id="last_post_link"></code></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.addEventListener('load', function () {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var obj = JSON.parse(this.responseText);
                    document.getElementById("name").innerHTML = obj.data.name;
                    document.getElementById("surname").innerHTML = obj.data.surname;
                    document.getElementById("post_count").innerHTML = obj.data.post_count;
                    document.getElementById("last_post_title").innerHTML = obj.data.last_post_title;
                    document.getElementById("last_post_link").innerHTML = obj.data.last_post_link;
                    document.getElementById("link_href").setAttribute('href', obj.data.last_post_link);
                }
            };
            xhttp.open("GET", "{{url('/').'/api/user/'.Auth::user()->id.'?api_token='.session('api_token')}}", true);
            xhttp.send();
        });
    </script>
</x-app-layout>
