<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Show Post') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <img style="width: 100%;" src="{{asset('storage/images') . '/' .  $post->img}}" alt="{{ $post->alt }}">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <p><strong>Title:</strong> {{ $post->title }}</p><br>
                            <p><strong>Alt:</strong> {{ $post->alt }}</p><br>
                            <a class="btn btn-primary" href="{{ url()->previous() }}">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
