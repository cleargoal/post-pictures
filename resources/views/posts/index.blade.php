<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Posts') }}
            @auth
                <a class="btn btn-success ml-1" href="{{ route('posts.create') }}">New Post</a>
            @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @elseif($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @foreach ($posts as $key => $post)
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-3">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row" style="margin-bottom: 2rem;">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <img style="width: 100%;" src="{{asset('storage/images') . '/' .  $post->img}}" alt="{{ $post->alt }}">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <p><strong>Title:</strong> {{ $post->title }}</p><br>
                            <p><strong>Alt:</strong> {{ $post->alt }}</p><br>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {!! $posts->links(); !!}
        </div>
    </div>
</x-app-layout>
