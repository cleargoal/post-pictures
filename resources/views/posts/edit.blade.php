<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Post') }}
        </h2>
    </x-slot>


    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('posts.update',$post->id) }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="image"><strong>Title:</strong></label>
                                    <input type="text" name="title" value="{{ $post->title }}" class="form-control" placeholder="Title"><br>
                                    <label for="image"><strong>Alt:</strong></label>
                                    <input type="text" name="alt" value="{{ $post->alt }}" class="form-control" placeholder="Alt"><br>
                                    <label for="img"><strong>Choose Image:</strong></label>
                                    <input id="img" type="file" name="img" class="form-control"><br>
                                    <label for="image"><strong>Image:</strong></label><br>
                                    <img src="{{asset('storage/images') . '/' .  $post->img}}" alt="{{ $post->alt }}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a class="btn btn-primary" href="{{ url()->previous() }}">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
