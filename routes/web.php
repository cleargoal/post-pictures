<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::resource('posts', PostController::class);

Route::get('user/edit', [UserController::class, 'edit'])->name('user.edit');

Route::get('user/profile', [UserController::class, 'profile'])->name('user.profile');

Route::put('user', [UserController::class, 'update'])->name('user.update');

require __DIR__.'/auth.php';
